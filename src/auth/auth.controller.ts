import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Post,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthService } from './services/auth.service';
import { LocalAuthGuard } from './local/local-auth.guard';
import { Public } from './jwt/jwt-constants';
import { CreateUserDto } from '../users/dto/createUser.dto';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { User } from '../users/entities/user.entity';
import { LoginDto } from './dto/login.dto';
import { ResponseLoginDto } from './dto/response-login.dto';
import { ResponseRegisterDto } from './dto/response-registration.dto';

@ApiTags('auth')
@Public()
@UseInterceptors(ClassSerializerInterceptor)
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOkResponse({
    description: 'Authorization was successful',
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized',
  })
  @UseGuards(LocalAuthGuard)
  @Post('login')
  @ApiBody({ type: LoginDto })
  async login(@Request() req: { user: User }): Promise<ResponseLoginDto> {
    return this.authService.login(req.user);
  }

  @ApiCreatedResponse({
    type: User,
    description: 'The user has been successfully created',
  })
  @ApiBadRequestResponse({
    description: 'This user already exists',
  })
  @ApiBody({ type: CreateUserDto })
  @Post('registration')
  async registration(@Body() dto: CreateUserDto): Promise<ResponseRegisterDto> {
    return this.authService.registration(dto);
  }
}
