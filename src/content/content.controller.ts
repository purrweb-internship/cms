import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ContentsService } from './content.service';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { ContentResponseDto } from './dto/content-response.dto';
import { CreateContentDto } from './dto/createContent.dto';
import { User } from '../users/entities/user.entity';
import { Content } from './entities/content.entity';
import { IsOwnerContentGuard } from './guards/is-owner-content.guard';
import { UploadFileResponseDto } from '../files/dto/upload-file-response.dto';
import { CreateFileDto } from '../files/dto/create-file.dto';

@ApiTags('content')
@ApiBearerAuth('JWT-auth')
@ApiUnauthorizedResponse({
  description: 'Unauthorized',
})
@Controller('content')
export class ContentsController {
  constructor(public service: ContentsService) {}

  @ApiCreatedResponse({
    type: ContentResponseDto,
    description: 'Content created successfully',
  })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiBody({ type: CreateContentDto })
  @Post()
  create(
    @Request() req: { user: User },
    @Body() dto: CreateContentDto,
  ): Promise<ContentResponseDto> {
    return this.service.create(req.user.id, dto);
  }

  @ApiOkResponse({
    type: Content,
    isArray: true,
    description: 'Get all content',
  })
  @Get('')
  findAll(): Promise<Content[]> {
    return this.service.findAll();
  }

  @ApiOkResponse({
    type: Content,
    description: 'Get one content',
  })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiParam({ name: 'id', type: Number })
  @Get(':id')
  findOne(
    @Param('id', ParseIntPipe) contentId: Content['id'],
  ): Promise<Content> {
    return this.service.findOne(contentId);
  }

  @UseGuards(IsOwnerContentGuard)
  @ApiOkResponse({
    type: UploadFileResponseDto,
    isArray: true,
    description: 'Add files',
  })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiBody({ type: CreateFileDto, isArray: true })
  @ApiParam({ name: 'id', type: Number })
  @Put(':id/files')
  addFiles(
    @Param('id', ParseIntPipe) contentId: Content['id'],
    @Body() filesDto: CreateFileDto[],
  ): Promise<UploadFileResponseDto[]> {
    return this.service.addFiles(contentId, filesDto);
  }

  @UseGuards(IsOwnerContentGuard)
  @ApiOkResponse({
    type: Boolean,
    description: 'Delete one content',
  })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiParam({ name: 'id', type: Number })
  @Delete(':id')
  remove(
    @Param('id', ParseIntPipe) contentId: Content['id'],
  ): Promise<boolean> {
    return this.service.remove(contentId);
  }

  @UseGuards(IsOwnerContentGuard)
  @ApiOkResponse({
    type: Boolean,
    description: 'Delete one file',
  })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiParam({ name: 'id', type: Number })
  @ApiParam({ name: 'fileKey', type: String })
  @Delete(':id/:fileKey')
  removeFile(
    @Param('id', ParseIntPipe) contentId: Content['id'],
    @Param('fileKey') fileKey: string,
  ): Promise<boolean> {
    return this.service.removeFile(contentId, fileKey);
  }
}
