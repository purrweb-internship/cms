import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Content } from './entities/content.entity';
import { ContentsService } from './content.service';
import { ContentsController } from './content.controller';
import { FilesModule } from '../files/files.module';
import { FilesCloudService } from '../files/servises/files-cloud.service';

@Module({
  imports: [TypeOrmModule.forFeature([Content]), FilesModule],
  providers: [ContentsService, FilesCloudService],
  exports: [ContentsService],
  controllers: [ContentsController],
})
export class ContentModule {}
