import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Content } from './entities/content.entity';
import { Repository } from 'typeorm';
import { UploadFileResponseDto } from '../files/dto/upload-file-response.dto';
import { CreateFileDto } from '../files/dto/create-file.dto';
import { FilesCloudService } from '../files/servises/files-cloud.service';
import { CreateContentDto } from './dto/createContent.dto';
import { ContentResponseDto } from './dto/content-response.dto';
import { User } from '../users/entities/user.entity';

@Injectable()
export class ContentsService {
  constructor(
    @InjectRepository(Content) private repo: Repository<Content>,
    private filesService: FilesCloudService,
  ) {}

  async create(
    userId: User['id'],
    dto: CreateContentDto,
  ): Promise<ContentResponseDto> {
    const filesResponseDto: UploadFileResponseDto[] =
      await this.filesService.saveFilesInCloud(dto.files);
    const keys: string[] = filesResponseDto.map((file) => file.key);

    const content = await this.repo.save({
      ...dto,
      userId: userId,
      files: keys,
    });

    return { content: content, files: filesResponseDto };
  }

  findAll(): Promise<Content[]> {
    return this.repo.find();
  }

  findOne(contentId: Content['id']): Promise<Content> {
    return this.repo.findOneOrFail(contentId);
  }

  async addFiles(
    contentId: Content['id'],
    filesDto: CreateFileDto[],
  ): Promise<UploadFileResponseDto[]> {
    const content: Content = await this.findOne(contentId);
    const uploadResponse: UploadFileResponseDto[] =
      await this.filesService.saveFilesInCloud(filesDto);
    const keys: string[] = uploadResponse.map((file) => file.key);

    await this.repo.save({ ...content, files: [...content.files, ...keys] });
    return uploadResponse;
  }

  async removeFile(contentId: Content['id'], key: string): Promise<boolean> {
    const content = await this.repo.findOneOrFail(contentId);
    await this.filesService.removeFileFromCloud(key);

    await this.repo.save({
      ...content,
      files: content.files.filter((keyExist) => keyExist != key),
    });
    return true;
  }

  async remove(contentId: Content['id']): Promise<boolean> {
    const content: Content = await this.findOne(contentId);
    await this.filesService.removeFilesFromCloud(content.files);
    await this.repo.remove(content);
    return true;
  }
}
