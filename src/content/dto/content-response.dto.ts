import { Content } from '../entities/content.entity';
import { UploadFileResponseDto } from '../../files/dto/upload-file-response.dto';
import { ApiProperty } from '@nestjs/swagger';

export class ContentResponseDto {
  @ApiProperty({ type: Content })
  content!: Content;

  @ApiProperty({ type: UploadFileResponseDto, isArray: true })
  files!: UploadFileResponseDto[];
}
