import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { PlaylistContent } from '../../playlist-content/entities/playlist-content.entity';
import { User } from '../../users/entities/user.entity';
import { IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ContentType } from './content-type.enum';
import { BaseEntity } from '../../base/entities/base-entity';

@Entity('content')
export class Content extends BaseEntity {
  @ApiProperty({
    description: 'Content type',
    enum: ContentType,
  })
  @Column({ enum: ContentType })
  @IsEnum(ContentType)
  contentType!: ContentType;

  @Column('int')
  userId!: User['id'];

  @ManyToOne(() => User, (user) => user.content, {
    onDelete: 'CASCADE',
  })
  user!: User;

  @OneToMany(
    () => PlaylistContent,
    (playlistContent) => playlistContent.content,
  )
  playlistContent!: PlaylistContent[];

  @ApiProperty({
    description: 'Content files',
    type: String,
    isArray: true,
  })
  @Column('varchar', { array: true, default: [] })
  files!: string[];
}
