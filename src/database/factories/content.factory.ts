import { define } from 'typeorm-seeding';
import { Content } from '../../content/entities/content.entity';
import { Faker } from '@faker-js/faker';
import { ContentType } from '../../content/entities/content-type.enum';
import { getRandomEnumValue } from '../../utils/utils';

define(Content, (faker: Faker) => {
  const content = new Content();

  content.contentType = getRandomEnumValue(ContentType);
  for (let i = 0; i < 5; i += 1) {
    content.files.push(faker.internet.url());
  }

  return content;
});
