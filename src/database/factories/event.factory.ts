import { define, factory } from 'typeorm-seeding';
import { Event } from '../../events/entities/event.entity';
import { Faker } from '@faker-js/faker';
import { User } from '../../users/entities/user.entity';

define(Event, (faker: Faker) => {
  const event = new Event();

  event.name = faker.lorem.word(10);
  event.user = factory(User)() as any;

  return event;
});
