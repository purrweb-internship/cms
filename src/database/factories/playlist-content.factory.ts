import { define } from 'typeorm-seeding';
import { PlaylistContent } from '../../playlist-content/entities/playlist-content.entity';
import { Faker } from '@faker-js/faker';

define(PlaylistContent, (faker: Faker) => {
  const playlistContent = new PlaylistContent();

  playlistContent.position = faker.random.number(100);
  playlistContent.duration = faker.random.number({ precision: 0.1 });

  return playlistContent;
});
