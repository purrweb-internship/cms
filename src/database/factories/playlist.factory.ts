import { define } from 'typeorm-seeding';
import { Playlist } from '../../playlists/entities/playlist.entity';
import { Faker } from '@faker-js/faker';

define(Playlist, (faker: Faker) => {
  const playlist = new Playlist();

  playlist.name = faker.lorem.word(10);

  return playlist;
});
