import { define } from 'typeorm-seeding';
import { Screen } from '../../screens/entities/screen.entity';
import { Faker } from '@faker-js/faker';

define(Screen, (faker: Faker) => {
  const screen = new Screen();

  screen.name = faker.lorem.word(10);
  return screen;
});
