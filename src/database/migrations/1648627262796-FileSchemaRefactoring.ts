import {MigrationInterface, QueryRunner} from "typeorm";

export class FileSchemaRefactoring1648627262796 implements MigrationInterface {
    name = 'FileSchemaRefactoring1648627262796'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "files" DROP CONSTRAINT "FK_b0f4fc1ad36940072271a1dbe9d"`);
        await queryRunner.query(`ALTER TABLE "files" DROP CONSTRAINT "UQ_2a26d04373d1dcc04c7f7aee214"`);
        await queryRunner.query(`ALTER TABLE "files" DROP COLUMN "url"`);
        await queryRunner.query(`ALTER TABLE "files" DROP COLUMN "mimeType"`);
        await queryRunner.query(`ALTER TABLE "files" ADD CONSTRAINT "FK_b0f4fc1ad36940072271a1dbe9d" FOREIGN KEY ("contentId") REFERENCES "content"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "files" DROP CONSTRAINT "FK_b0f4fc1ad36940072271a1dbe9d"`);
        await queryRunner.query(`ALTER TABLE "files" ADD "mimeType" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "files" ADD "url" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "files" ADD CONSTRAINT "UQ_2a26d04373d1dcc04c7f7aee214" UNIQUE ("url")`);
        await queryRunner.query(`ALTER TABLE "files" ADD CONSTRAINT "FK_b0f4fc1ad36940072271a1dbe9d" FOREIGN KEY ("contentId") REFERENCES "content"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

}
