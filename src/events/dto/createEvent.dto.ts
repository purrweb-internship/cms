import { PickType } from '@nestjs/swagger';
import { Event } from '../entities/event.entity';

export class CreateEventDto extends PickType(Event, ['name']) {}
