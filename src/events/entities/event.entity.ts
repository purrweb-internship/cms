import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { Screen } from '../../screens/entities/screen.entity';
import { IsString, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from '../../base/entities/base-entity';

@Entity('events')
export class Event extends BaseEntity {
  @ApiProperty({
    example: 'Event name',
    minLength: 2,
    maxLength: 20,
  })
  @Column('varchar')
  @IsString()
  @Length(2, 20)
  name!: string;

  @Column('int')
  userId!: User['id'];

  @ManyToOne(() => User, (user) => user.events, {
    onDelete: 'CASCADE',
  })
  user!: User;

  @OneToMany(() => Screen, (screen) => screen.event)
  screens!: Screen[];
}
