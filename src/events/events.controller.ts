import { Controller, UseGuards } from '@nestjs/common';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { Event } from './entities/event.entity';
import { EventsService } from './events.service';
import { CreateEventDto } from './dto/createEvent.dto';
import { UpdateEventDto } from './dto/updateEvent.dto';
import { User } from '../users/entities/user.entity';
import { IsOwnerEventGuard } from './guards/is-owner-event.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('events')
@ApiBearerAuth('JWT-auth')
@Crud({
  model: {
    type: Event,
  },
  dto: {
    create: CreateEventDto,
    update: UpdateEventDto,
    replace: CreateEventDto,
  },
  query: {
    join: {
      screens: {
        eager: true,
      },
    },
  },
  routes: {
    updateOneBase: {
      decorators: [UseGuards(IsOwnerEventGuard)],
    },
    replaceOneBase: {
      decorators: [UseGuards(IsOwnerEventGuard)],
    },
    deleteOneBase: {
      decorators: [UseGuards(IsOwnerEventGuard)],
    },
  },
})
@CrudAuth({
  property: 'user',
  persist: (user: User) => ({
    userId: user.id,
  }),
})
@Controller('events')
export class EventsController implements CrudController<Event> {
  constructor(public service: EventsService) {}
}
