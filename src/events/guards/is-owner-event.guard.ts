import { IsOwnerGuard } from '../../base/guards/is-owner.guard';
import { Event } from '../entities/event.entity';

export const IsOwnerEventGuard = new IsOwnerGuard(Event);
