import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { IsImage } from '../validators/is-image.validator';

export class CreateFileDto {
  @ApiProperty()
  @IsString()
  @IsImage()
  filename!: string;
}
