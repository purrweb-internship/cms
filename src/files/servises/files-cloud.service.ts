import { Inject, Injectable } from '@nestjs/common';
import { v4 } from 'uuid';
import { UploadFileResponseDto } from '../dto/upload-file-response.dto';
import {
  DeleteObjectCommand,
  PutObjectCommand,
  PutObjectCommandInput,
  S3Client,
} from '@aws-sdk/client-s3';
import { ConfigService } from '@nestjs/config';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';
import { CreateFileDto } from '../dto/create-file.dto';

@Injectable()
export class FilesCloudService {
  private readonly s3Client: S3Client;

  constructor(@Inject(ConfigService) private configService: ConfigService) {
    this.s3Client = new S3Client({
      region: 'ru-central1',
      credentials: {
        accessKeyId: configService.get('YANDEX_ACCESS_KEY_ID') as string,
        secretAccessKey: configService.get(
          'YANDEX_SECRET_ACCESS_KEY',
        ) as string,
      },
      apiVersion: 'latest',
      endpoint: 'https://storage.yandexcloud.net',
    });
  }

  async saveFileInCloud(dto: CreateFileDto): Promise<UploadFileResponseDto> {
    const hash: string = v4();
    const newKey: string = hash + dto.filename;

    const bucketParams: PutObjectCommandInput = {
      Bucket: this.configService.get('YANDEX_BUCKET_NAME'),
      Key: newKey,
    };
    const command = new PutObjectCommand(bucketParams);
    const signedUrl = await getSignedUrl(this.s3Client, command);

    return {
      signedUrl: signedUrl,
      key: newKey,
    };
  }

  async saveFilesInCloud(
    dtos: CreateFileDto[],
  ): Promise<UploadFileResponseDto[]> {
    if (!dtos) {
      return [];
    }
    return Promise.all(dtos.map((dto) => this.saveFileInCloud(dto)));
  }

  async removeFileFromCloud(key: string): Promise<boolean> {
    await this.s3Client.send(
      new DeleteObjectCommand({
        Bucket: this.configService.get('YANDEX_BUCKET_NAME'),
        Key: key,
      }),
    );
    return true;
  }

  async removeFilesFromCloud(keys: string[]): Promise<boolean> {
    await Promise.all(keys.map((key) => this.removeFileFromCloud(key)));
    return true;
  }
}
