import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import path = require('path');

@ValidatorConstraint({ name: 'CheckExtension', async: true })
@Injectable()
export class CheckExtensionValidator implements ValidatorConstraintInterface {
  private readonly validExt!: string[];

  constructor(validExt: string[]) {
    this.validExt = validExt;
  }

  validate(filename: string): boolean {
    const ext = path.extname(filename).substring(1);

    return this.validExt.includes(ext.toLowerCase());
  }

  defaultMessage(): string {
    return 'Invalid file extension';
  }
}
