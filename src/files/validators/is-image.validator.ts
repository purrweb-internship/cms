import { CheckExtensionValidator } from './check-extension.validator';
import { registerDecorator, ValidationOptions } from 'class-validator';

const VALID_IMAGES_EXTENSIONS: string[] = ['jpg', 'jpeg', 'png'];

const IsImageValidator = new CheckExtensionValidator(VALID_IMAGES_EXTENSIONS);

export function IsImage(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IsImage',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsImageValidator,
    });
  };
}
