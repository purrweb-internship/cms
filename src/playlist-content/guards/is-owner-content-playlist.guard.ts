import { IsOwnerGuard } from '../../base/guards/is-owner.guard';
import { Content } from '../../content/entities/content.entity';

export const IsOwnerContentPlaylistGuard = new IsOwnerGuard(
  Content,
  'contentId',
);
