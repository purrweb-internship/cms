import { IsOwnerGuard } from '../../base/guards/is-owner.guard';
import { Playlist } from '../../playlists/entities/playlist.entity';

export const IsOwnerPlaylistContentGuard = new IsOwnerGuard(
  Playlist,
  'playlistId',
);
