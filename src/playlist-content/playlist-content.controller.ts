import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { Controller, UseGuards } from '@nestjs/common';
import { CreatePlaylistContentDto } from './dto/createPlaylistContent.dto';
import { UpdatePlaylistContentDto } from './dto/updatePlaylistContent.dto';
import { PlaylistContent } from './entities/playlist-content.entity';
import { IsOwnerPlaylistContentGuard } from './guards/is-owner-playlist-content.guard';
import { IsOwnerContentPlaylistGuard } from './guards/is-owner-content-playlist.guard';
import { User } from '../users/entities/user.entity';
import { PlaylistContentService } from './playlist-content.service';

@ApiTags('PlaylistContent')
@ApiBearerAuth('JWT-auth')
@Crud({
  model: {
    type: PlaylistContent,
  },
  dto: {
    create: CreatePlaylistContentDto,
    update: UpdatePlaylistContentDto,
    replace: CreatePlaylistContentDto,
  },
  params: {
    playlistId: {
      field: 'playlistId',
      type: 'number',
      primary: true,
    },
    contentId: {
      field: 'contentId',
      type: 'number',
      primary: true,
    },
  },
  query: {
    join: {
      content: {
        eager: true,
      },
      playlist: {
        eager: true,
      },
    },
  },

  routes: {
    exclude: ['createManyBase'],
    createManyBase: {
      decorators: [
        UseGuards(IsOwnerPlaylistContentGuard),
        UseGuards(IsOwnerContentPlaylistGuard),
      ],
    },
    createOneBase: {
      decorators: [
        UseGuards(IsOwnerPlaylistContentGuard),
        UseGuards(IsOwnerContentPlaylistGuard),
      ],
    },
    updateOneBase: {
      decorators: [
        UseGuards(IsOwnerPlaylistContentGuard),
        UseGuards(IsOwnerContentPlaylistGuard),
      ],
    },
    deleteOneBase: {
      decorators: [
        UseGuards(IsOwnerPlaylistContentGuard),
        UseGuards(IsOwnerContentPlaylistGuard),
      ],
    },
  },
})
@CrudAuth({
  property: 'user',
  persist: (user: User) => ({
    userId: user.id,
  }),
})
@Controller('playlist-content')
export class PlaylistContentController
  implements CrudController<PlaylistContent>
{
  constructor(public service: PlaylistContentService) {}
}
