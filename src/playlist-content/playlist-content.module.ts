import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlaylistContent } from './entities/playlist-content.entity';
import { PlaylistContentService } from './playlist-content.service';
import { PlaylistContentController } from './playlist-content.controller';

@Module({
  imports: [TypeOrmModule.forFeature([PlaylistContent])],
  providers: [PlaylistContentService],
  exports: [PlaylistContentService],
  controllers: [PlaylistContentController],
})
export class PlaylistContentModule {}
