import { IsOwnerGuard } from '../../base/guards/is-owner.guard';
import { Screen } from '../../screens/entities/screen.entity';

export const IsOwnerParentPlaylistGuard = new IsOwnerGuard(Screen, 'screenId');
