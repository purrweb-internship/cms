import { PartialType } from '@nestjs/swagger';
import { CreateScreenDto } from './createScreen.dto';

export class UpdateScreenDto extends PartialType(CreateScreenDto) {}
