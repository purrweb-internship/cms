import { Column, Entity, ManyToOne, OneToOne } from 'typeorm';
import { Event } from '../../events/entities/event.entity';
import { Playlist } from '../../playlists/entities/playlist.entity';
import { User } from '../../users/entities/user.entity';
import { IsString, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from '../../base/entities/base-entity';

@Entity('screens')
export class Screen extends BaseEntity {
  @ApiProperty({
    example: 'screen name',
    minLength: 2,
    maxLength: 20,
  })
  @Column('varchar')
  @IsString()
  @Length(2, 20)
  name!: string;

  @Column('int')
  userId!: User['id'];

  @ManyToOne(() => User, (user) => user.screens, {
    onDelete: 'CASCADE',
  })
  user!: User;

  @ApiProperty({ type: Number })
  @Column('int')
  eventId!: Event['id'];

  @ManyToOne(() => Event, (event) => event.screens, {
    onDelete: 'CASCADE',
  })
  event!: Event;

  @OneToOne(() => Playlist)
  playlist?: Playlist;
}
