import { IsOwnerGuard } from '../../base/guards/is-owner.guard';
import { Screen } from '../entities/screen.entity';

export const IsOwnerScreenGuard = new IsOwnerGuard(Screen);
