import { Controller, UseGuards } from '@nestjs/common';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { Screen } from './entities/screen.entity';
import { ScreensService } from './screens.service';
import { CreateScreenDto } from './dto/createScreen.dto';
import { UpdateScreenDto } from './dto/updateScreen.dto';
import { User } from '../users/entities/user.entity';
import { IsOwnerScreenGuard } from './guards/is-owner-screen.guard';
import { IsOwnerParentScreenGuard } from './guards/is-owner-parent-screen.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('screens')
@ApiBearerAuth('JWT-auth')
@Crud({
  model: {
    type: Screen,
  },
  dto: {
    create: CreateScreenDto,
    update: UpdateScreenDto,
    replace: CreateScreenDto,
  },
  params: {
    eventId: {
      field: 'eventId',
      type: 'number',
    },
  },
  routes: {
    createOneBase: {
      decorators: [UseGuards(IsOwnerParentScreenGuard)],
    },
    createManyBase: {
      decorators: [UseGuards(IsOwnerParentScreenGuard)],
    },
    updateOneBase: {
      decorators: [UseGuards(IsOwnerScreenGuard)],
    },
    replaceOneBase: {
      decorators: [UseGuards(IsOwnerScreenGuard)],
    },
    deleteOneBase: {
      decorators: [UseGuards(IsOwnerScreenGuard)],
    },
  },
})
@CrudAuth({
  property: 'user',
  persist: (user: User) => ({
    userId: user.id,
  }),
})
@Controller('events/:eventId/screens')
export class ScreensController implements CrudController<Screen> {
  constructor(public service: ScreensService) {}
}
