import { IntersectionType } from '@nestjs/swagger';
import { CreateUserDto } from './createUser.dto';
import { UserPasswordDto } from './user-password.dto';

export class UpdateUserDto extends IntersectionType(
  CreateUserDto,
  UserPasswordDto,
) {}
