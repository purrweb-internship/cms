import { Column, Entity, OneToMany } from 'typeorm';
import { Event } from '../../events/entities/event.entity';
import { Screen } from '../../screens/entities/screen.entity';
import { Playlist } from '../../playlists/entities/playlist.entity';
import { Content } from '../../content/entities/content.entity';
import { BaseEntity } from '../../base/entities/base-entity';
import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

@Entity('users')
export class User extends BaseEntity {
  @ApiProperty({
    example: 'user@gmail.com',
  })
  @IsEmail()
  @Column('varchar', { unique: true })
  email!: string;

  @ApiProperty({
    example: 'password',
    minLength: 6,
    maxLength: 50,
  })
  @Exclude({ toPlainOnly: true })
  @Column('varchar')
  @IsString()
  @IsNotEmpty()
  @Length(6, 50)
  password!: string;

  @OneToMany(() => Event, (event) => event.user)
  events!: Event[];

  @OneToMany(() => Screen, (screen) => screen.user)
  screens!: Screen[];

  @OneToMany(() => Playlist, (playlist) => playlist.user)
  playlists!: Playlist[];

  @OneToMany(() => Content, (content) => content.user)
  content!: Content[];
}
