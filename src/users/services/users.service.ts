import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { User } from '../entities/user.entity';
import { HashService } from './hash.service';
import { CrudRequest } from '@nestjsx/crud';
import { UpdateUserDto } from '../dto/updateUser.dto';
import { CreateUserDto } from '../dto/createUser.dto';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService extends TypeOrmCrudService<User> {
  constructor(
    @InjectRepository(User) repo: Repository<User>,
    private hashService: HashService,
  ) {
    super(repo);
  }

  async updateOne(req: CrudRequest, dto: UpdateUserDto): Promise<User> {
    if (dto.password) {
      dto.password = await this.hashService.generateHash(dto.password);
    }
    return super.updateOne(req, dto);
  }

  async registerOne(dto: CreateUserDto): Promise<User> {
    dto.password = await this.hashService.generateHash(dto.password);
    return this.repo.save(dto);
  }
}
